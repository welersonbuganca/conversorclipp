
Imports FirebirdSql.Data.FirebirdClient

Public Class FB

    Public Sub Clientes(dbPathOrigem, dbPathDestino)

        Dim connStrOrigem = "User=SYSDBA;Password=masterkey;Database=" & dbPathOrigem & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connOrigem As FbConnection = New FbConnection(connStrOrigem)
        connOrigem.Open()

        Dim cmdSelect = New FbCommand("SELECT 
                                            ID_CLIENTE, ID_CONVENIO, DT_CADASTRO, NOME, END_CEP, END_TIPO, 
                                            END_NUMERO, END_LOGRAD, END_BAIRRO, END_COMPLE, DT_PRICOMP, DT_ULTCOMP, 
                                            CONTATO, STATUS, LIMITE, DDD_RESID, FONE_RESID, DDD_COMER, FONE_COMER, DDD_CELUL, 
                                            FONE_CELUL, DDD_FAX, FONE_FAX, EMAIL_CONT, EMAIL_NFE, ID_CIDADE, ID_TIPO, ID_FUNCIONARIO, 
                                            ID_PAIS, MENSAGEM, ID_RAMO, EMAIL_ADIC, OBSERVACAO, DT_MELHOR_VENCTO
                                        FROM TB_CLIENTE;", connOrigem)

        Dim dtreader As FbDataReader = cmdSelect.ExecuteReader

        Dim connStrDestino = "User=SYSDBA;Password=masterkey;Database=" & dbPathDestino & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connDestino As FbConnection = New FbConnection(connStrDestino)

        connDestino.Open()

        Dim fbcommand = New FbCommand("INSERT INTO TB_CLIENTE
                                     (ID_CLIENTE, ID_CONVENIO, DT_CADASTRO, NOME, END_CEP, END_TIPO, END_NUMERO,
                                     END_LOGRAD, END_BAIRRO, END_COMPLE, DT_PRICOMP, DT_ULTCOMP, CONTATO, STATUS, 
                                     LIMITE, DDD_RESID, FONE_RESID, DDD_COMER, FONE_COMER, DDD_CELUL, FONE_CELUL, 
                                     DDD_FAX, FONE_FAX, EMAIL_CONT, EMAIL_NFE, ID_CIDADE, ID_TIPO, ID_FUNCIONARIO, 
                                     ID_PAIS, MENSAGEM, ID_RAMO, EMAIL_ADIC, OBSERVACAO, DT_MELHOR_VENCTO)   
                                     VALUES
                                    (@ID_CLIENTE, @ID_CONVENIO, @DT_CADASTRO, @NOME, @END_CEP, @END_TIPO, @END_NUMERO,
                                     @END_LOGRAD, @END_BAIRRO, @END_COMPLE, @DT_PRICOMP, @DT_ULTCOMP, @CONTATO, @STATUS, 
                                     @LIMITE, @DDD_RESID, @FONE_RESID, @DDD_COMER, @FONE_COMER, @DDD_CELUL, @FONE_CELUL, 
                                     @DDD_FAX, @FONE_FAX, @EMAIL_CONT, @EMAIL_NFE, @ID_CIDADE, @ID_TIPO, @ID_FUNCIONARIO, 
                                     @ID_PAIS, @MENSAGEM, @ID_RAMO, @EMAIL_ADIC, @OBSERVACAO, @DT_MELHOR_VENCTO);", connDestino)

        While (dtreader.Read)
            If (dtreader.GetValue(0) = 0) Then
                ' Verifica se é o Clientes Diversos
            Else
                fbcommand.Parameters.AddWithValue("@ID_CLIENTE", dtreader.GetValue(0))
                fbcommand.Parameters.AddWithValue("@ID_CONVENIO", dtreader.GetValue(1))
                fbcommand.Parameters.AddWithValue("@DT_CADASTRO", dtreader.GetValue(2))
                fbcommand.Parameters.AddWithValue("@NOME", dtreader.GetValue(3))
                fbcommand.Parameters.AddWithValue("@END_CEP", dtreader.GetValue(4))
                fbcommand.Parameters.AddWithValue("@END_TIPO", dtreader.GetValue(5))
                fbcommand.Parameters.AddWithValue("@END_NUMERO", dtreader.GetValue(6))
                fbcommand.Parameters.AddWithValue("@END_LOGRAD", dtreader.GetValue(7))
                fbcommand.Parameters.AddWithValue("@END_BAIRRO", dtreader.GetValue(8))
                fbcommand.Parameters.AddWithValue("@END_COMPLE", dtreader.GetValue(9))
                fbcommand.Parameters.AddWithValue("@DT_PRICOMP", dtreader.GetValue(10))
                fbcommand.Parameters.AddWithValue("@DT_ULTCOMP", dtreader.GetValue(11))
                fbcommand.Parameters.AddWithValue("@CONTATO", dtreader.GetValue(12))
                fbcommand.Parameters.AddWithValue("@STATUS", dtreader.GetValue(13))
                fbcommand.Parameters.AddWithValue("@LIMITE", dtreader.GetValue(14))
                fbcommand.Parameters.AddWithValue("@DDD_RESID", dtreader.GetValue(15))
                fbcommand.Parameters.AddWithValue("@FONE_RESID", dtreader.GetValue(16))
                fbcommand.Parameters.AddWithValue("@DDD_COMER", dtreader.GetValue(17))
                fbcommand.Parameters.AddWithValue("@FONE_COMER", dtreader.GetValue(18))
                fbcommand.Parameters.AddWithValue("@DDD_CELUL", dtreader.GetValue(19))
                fbcommand.Parameters.AddWithValue("@FONE_CELUL", dtreader.GetValue(20))
                fbcommand.Parameters.AddWithValue("@DDD_FAX", dtreader.GetValue(21))
                fbcommand.Parameters.AddWithValue("@FONE_FAX", dtreader.GetValue(22))
                fbcommand.Parameters.AddWithValue("@EMAIL_CONT", dtreader.GetValue(23))
                fbcommand.Parameters.AddWithValue("@EMAIL_NFE", dtreader.GetValue(24))
                fbcommand.Parameters.AddWithValue("@ID_CIDADE", dtreader.GetValue(25))
                fbcommand.Parameters.AddWithValue("@ID_TIPO", dtreader.GetValue(26))
                fbcommand.Parameters.AddWithValue("@ID_FUNCIONARIO", dtreader.GetValue(27))
                fbcommand.Parameters.AddWithValue("@ID_PAIS", dtreader.GetValue(28))
                fbcommand.Parameters.AddWithValue("@MENSAGEM", dtreader.GetValue(29))
                fbcommand.Parameters.AddWithValue("@ID_RAMO", dtreader.GetValue(30))
                fbcommand.Parameters.AddWithValue("@EMAIL_ADIC", dtreader.GetValue(31))
                fbcommand.Parameters.AddWithValue("@OBSERVACAO", dtreader.GetValue(32))
                fbcommand.Parameters.AddWithValue("@DT_MELHOR_VENCTO", dtreader.GetValue(33))

                Try
                    fbcommand.Prepare()
                    fbcommand.ExecuteNonQuery()
                    fbcommand.Parameters.Clear()
                Catch ex As Exception
                    MsgBox(ex.Message, vbExclamation, "Opps... problemas na Conversão")
                    Conversor.listSaida.Items.Add("Error: " & ex.Message)
                End Try

            End If


        End While

        connOrigem.Close()

        Dim cmdCountID = New FbCommand("SELECT MAX(ID_CLIENTE) FROM TB_CLIENTE;", connDestino)
        Dim reader As FbDataReader = cmdCountID.ExecuteReader
        reader.Read()
        If Not IsDBNull(reader.GetValue(0)) Then
            Dim cmdUpdateId = New FbCommand("ALTER SEQUENCE GEN_TB_CLIENTE_ID RESTART WITH " & reader.GetValue(0), connDestino)
            cmdUpdateId.Prepare()
            cmdUpdateId.ExecuteNonQuery()
        End If


        connDestino.Close()

    End Sub

    Sub Cli_PF(dbPathOrigem, dbPathDestino)
        Dim connStrOrigem = "User=SYSDBA;Password=masterkey;Database=" & dbPathOrigem & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connOrigem As FbConnection = New FbConnection(connStrOrigem)
        connOrigem.Open()

        Dim cmdSelect = New FbCommand("SELECT 
                                            ID_CLIENTE, CPF, IDENTIDADE, NOME_PAI, NOME_MAE, PROFISSAO, DT_NASCTO, 
                                            RENDA, LOCAL_TRAB, DATA_ADM, NATUR, FOTO
                                      FROM TB_CLI_PF;", connOrigem)

        Dim dtreader As FbDataReader = cmdSelect.ExecuteReader

        Dim connStrDestino = "User=SYSDBA;Password=masterkey;Database=" & dbPathDestino & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connDestino As FbConnection = New FbConnection(connStrDestino)

        connDestino.Open()

        Dim fbcommand = New FbCommand("INSERT INTO TB_CLI_PF 
                                        (ID_CLIENTE, CPF, IDENTIDADE, NOME_PAI, NOME_MAE, PROFISSAO, DT_NASCTO, RENDA, 
                                          LOCAL_TRAB, DATA_ADM, NATUR, FOTO) 
                                     VALUES 
                                        (@ID_CLIENTE, @CPF, @IDENTIDADE, @NOME_PAI, @NOME_MAE, @PROFISSAO, @DT_NASCTO, 
                                         @RENDA, @LOCAL_TRAB, @DATA_ADM, @NATUR, @FOTO);", connDestino)

        While (dtreader.Read)
            If (dtreader.GetValue(0) = 0) Then
                ' Verifica se é o Clientes Diversos
            Else
                fbcommand.Parameters.AddWithValue("@ID_CLIENTE", dtreader.GetValue(0))
                fbcommand.Parameters.AddWithValue("@CPF", dtreader.GetValue(1))
                fbcommand.Parameters.AddWithValue("@IDENTIDADE", dtreader.GetValue(2))
                fbcommand.Parameters.AddWithValue("@NOME_PAI", dtreader.GetValue(3))
                fbcommand.Parameters.AddWithValue("@NOME_MAE", dtreader.GetValue(4))
                fbcommand.Parameters.AddWithValue("@PROFISSAO", dtreader.GetValue(5))
                fbcommand.Parameters.AddWithValue("@DT_NASCTO", dtreader.GetValue(6))
                fbcommand.Parameters.AddWithValue("@RENDA", dtreader.GetValue(7))
                fbcommand.Parameters.AddWithValue("@LOCAL_TRAB", dtreader.GetValue(8))
                fbcommand.Parameters.AddWithValue("@DATA_ADM", dtreader.GetValue(9))
                fbcommand.Parameters.AddWithValue("@NATUR", dtreader.GetValue(10))
                fbcommand.Parameters.AddWithValue("@FOTO", dtreader.GetValue(11))

                Try
                    fbcommand.Prepare()
                    fbcommand.ExecuteNonQuery()
                    fbcommand.Parameters.Clear()
                Catch ex As Exception
                    MsgBox(ex.Message, vbExclamation, "Opps... problemas na Conversão")
                    Conversor.listSaida.Items.Add("Error: " & ex.Message)
                End Try

            End If

        End While
        connOrigem.Close()
        connDestino.Close()

    End Sub

    Sub Cli_PF_Adicionais(dbPathOrigem, dbPathDestino)
        Dim connStrOrigem = "User=SYSDBA;Password=masterkey;Database=" & dbPathOrigem & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connOrigem As FbConnection = New FbConnection(connStrOrigem)
        connOrigem.Open()

        Dim cmdSelect = New FbCommand("SELECT 
                                        ID_CLIENTE, NOME_CONJU, NASC_CONJU, CPF_CONJU, NUM_DEPEN, CASA_PROPR, CASA_FINAN
                                       FROM TB_CLI_PF_ADICIONAIS;", connOrigem)

        Dim dtreader As FbDataReader = cmdSelect.ExecuteReader

        Dim connStrDestino = "User=SYSDBA;Password=masterkey;Database=" & dbPathDestino & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connDestino As FbConnection = New FbConnection(connStrDestino)

        connDestino.Open()

        Dim fbcommand = New FbCommand("INSERT INTO TB_CLI_PF_ADICIONAIS (ID_CLIENTE, NOME_CONJU, NASC_CONJU, 
                                          CPF_CONJU, NUM_DEPEN, CASA_PROPR, CASA_FINAN) 
                                       VALUES 
                                          (@ID_CLIENTE, @NOME_CONJU, @NASC_CONJU, @CPF_CONJU, 
                                           @NUM_DEPEN, @CASA_PROPR, @CASA_FINAN);", connDestino)
        While (dtreader.Read)
            If (dtreader.GetValue(0) = 0) Then
                ' Verifica se é o Clientes Diversos
            Else
                fbcommand.Parameters.AddWithValue("@ID_CLIENTE", dtreader.GetValue(0))
                fbcommand.Parameters.AddWithValue("@NOME_CONJU", dtreader.GetValue(1))
                fbcommand.Parameters.AddWithValue("@NASC_CONJU", dtreader.GetValue(2))
                fbcommand.Parameters.AddWithValue("@CPF_CONJU", dtreader.GetValue(3))
                fbcommand.Parameters.AddWithValue("@NUM_DEPEN", dtreader.GetValue(4))
                fbcommand.Parameters.AddWithValue("@CASA_PROPR", dtreader.GetValue(5))
                fbcommand.Parameters.AddWithValue("@CASA_FINAN", dtreader.GetValue(6))

                Try
                    fbcommand.Prepare()
                    fbcommand.ExecuteNonQuery()
                    fbcommand.Parameters.Clear()
                Catch ex As Exception
                    MsgBox(ex.Message, vbExclamation, "Opps... problemas na Conversão")
                    Conversor.listSaida.Items.Add("Error: " & ex.Message)
                End Try

            End If

        End While
        connOrigem.Close()
        connDestino.Close()

    End Sub

    Sub Cli_PJ(dbPathOrigem, dbPathDestino)

        Dim connStrOrigem = "User=SYSDBA;Password=masterkey;Database=" & dbPathOrigem & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connOrigem As FbConnection = New FbConnection(connStrOrigem)
        connOrigem.Open()

        Dim cmdSelect = New FbCommand("SELECT 
                                             ID_CLIENTE, CNPJ, NOME_FANTA, INSC_ESTAD, INSC_MUNIC, SOC_GERENTE, 
                                             IND_IE_DEST, INSC_SUFRAMA
                                       FROM TB_CLI_PJ;", connOrigem)

        Dim dtreader As FbDataReader = cmdSelect.ExecuteReader

        Dim connStrDestino = "User=SYSDBA;Password=masterkey;Database=" & dbPathDestino & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connDestino As FbConnection = New FbConnection(connStrDestino)

        connDestino.Open()

        Dim fbcommand = New FbCommand("INSERT INTO TB_CLI_PJ 
                                        (ID_CLIENTE, CNPJ, NOME_FANTA, INSC_ESTAD, INSC_MUNIC, SOC_GERENTE, 
                                                IND_IE_DEST, INSC_SUFRAMA) 
                                        VALUES 
                                        (@ID_CLIENTE, @CNPJ, @NOME_FANTA, @INSC_ESTAD, @INSC_MUNIC, @SOC_GERENTE, 
                                                @IND_IE_DEST, @INSC_SUFRAMA);", connDestino)
        While (dtreader.Read)
            If (dtreader.GetValue(0) = 0) Then
                ' Verifica se é o Clientes Diversos
            Else
                fbcommand.Parameters.AddWithValue("@ID_CLIENTE", dtreader.GetValue(0))
                fbcommand.Parameters.AddWithValue("@CNPJ", dtreader.GetValue(1))
                fbcommand.Parameters.AddWithValue("@NOME_FANTA", dtreader.GetValue(2))
                fbcommand.Parameters.AddWithValue("@INSC_ESTAD", dtreader.GetValue(3))
                fbcommand.Parameters.AddWithValue("@INSC_MUNIC", dtreader.GetValue(4))
                fbcommand.Parameters.AddWithValue("@SOC_GERENTE", dtreader.GetValue(5))
                fbcommand.Parameters.AddWithValue("@IND_IE_DEST", dtreader.GetValue(6))
                fbcommand.Parameters.AddWithValue("@INSC_SUFRAMA", dtreader.GetValue(7))

                Try
                    fbcommand.Prepare()
                    fbcommand.ExecuteNonQuery()
                    fbcommand.Parameters.Clear()
                Catch ex As Exception
                    MsgBox(ex.Message, vbExclamation, "Opps... problemas na Conversão")
                    Conversor.listSaida.Items.Add("Error: " & ex.Message)
                End Try

            End If

        End While
        connOrigem.Close()
        connDestino.Close()

    End Sub

    Sub Estoque(dbPathOrigem, dbPathDestino)

        Dim connStrOrigem = "User=SYSDBA;Password=masterkey;Database=" & dbPathOrigem & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connOrigem As FbConnection = New FbConnection(connStrOrigem)
        connOrigem.Open()

        Dim cmdSelect = New FbCommand("SELECT 
                                            ID_ESTOQUE, ID_GRUPO, DESCRICAO, STATUS, DT_CADAST, HR_CADAST, FRACIONADO, 
                                            PRC_VENDA, PRC_CUSTO, ULT_VENDA, MARGEM_LB, POR_COMISSAO, 
                                            GRADE_SERIE, ID_TIPOITEM, ID_CTI, CST_PIS, CST_COFINS, PIS, COFINS, UNI_MEDIDA,
                                            MARGEM_PV, CFOP, OBSERVACAO, NAT_RECEITA, CFOP_NF, PRC_ATACADO, ID_CTI_PART, 
                                            ID_CTI_FCP, QTD_ATACADO
                                       FROM TB_ESTOQUE;", connOrigem)

        Dim dtreader As FbDataReader = cmdSelect.ExecuteReader

        Dim connStrDestino = "User=SYSDBA;Password=masterkey;Database=" & dbPathDestino & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connDestino As FbConnection = New FbConnection(connStrDestino)

        connDestino.Open()

        Dim fbcommand = New FbCommand("INSERT INTO TB_ESTOQUE 
                                            (ID_ESTOQUE, ID_GRUPO, DESCRICAO, STATUS, DT_CADAST, HR_CADAST, FRACIONADO, 
                                            PRC_VENDA, PRC_CUSTO, ULT_VENDA, MARGEM_LB, POR_COMISSAO, 
                                            GRADE_SERIE, ID_TIPOITEM, ID_CTI, CST_PIS, CST_COFINS, PIS, COFINS, UNI_MEDIDA,
                                            MARGEM_PV, CFOP, OBSERVACAO, NAT_RECEITA, CFOP_NF, PRC_ATACADO, ID_CTI_PART, 
                                            ID_CTI_FCP, QTD_ATACADO) 
                                      VALUES 
                                            (@ID_ESTOQUE, @ID_GRUPO, @DESCRICAO, @STATUS, @DT_CADAST, @HR_CADAST, @FRACIONADO, 
                                            @PRC_VENDA, @PRC_CUSTO, @ULT_VENDA, @MARGEM_LB, @POR_COMISSAO, 
                                            @GRADE_SERIE, @ID_TIPOITEM, @ID_CTI, @CST_PIS, @CST_COFINS, @PIS, @COFINS, @UNI_MEDIDA,
                                            @MARGEM_PV, @CFOP, @OBSERVACAO, @NAT_RECEITA, @CFOP_NF, @PRC_ATACADO, @ID_CTI_PART, 
                                            @ID_CTI_FCP, @QTD_ATACADO);", connDestino)

        While (dtreader.Read)
            If (dtreader.GetValue(0) = 0) Then
                ' Verifica se é o Clientes Diversos
            Else
                fbcommand.Parameters.AddWithValue("@ID_ESTOQUE", dtreader.GetValue(0))
                fbcommand.Parameters.AddWithValue("@ID_GRUPO", dtreader.GetValue(1))
                fbcommand.Parameters.AddWithValue("@DESCRICAO", dtreader.GetValue(2))
                fbcommand.Parameters.AddWithValue("@STATUS", dtreader.GetValue(3))
                fbcommand.Parameters.AddWithValue("@DT_CADAST", dtreader.GetValue(4))
                fbcommand.Parameters.AddWithValue("@HR_CADAST", dtreader.GetValue(5))
                fbcommand.Parameters.AddWithValue("@FRACIONADO", dtreader.GetValue(6))
                fbcommand.Parameters.AddWithValue("@PRC_VENDA", dtreader.GetValue(7))
                fbcommand.Parameters.AddWithValue("@PRC_CUSTO", dtreader.GetValue(8))
                fbcommand.Parameters.AddWithValue("@ULT_VENDA", dtreader.GetValue(9))
                fbcommand.Parameters.AddWithValue("@MARGEM_LB", dtreader.GetValue(10))
                fbcommand.Parameters.AddWithValue("@POR_COMISSAO", dtreader.GetValue(11))
                fbcommand.Parameters.AddWithValue("@GRADE_SERIE", dtreader.GetValue(12))
                fbcommand.Parameters.AddWithValue("@ID_TIPOITEM", dtreader.GetValue(13))
                fbcommand.Parameters.AddWithValue("@ID_CTI", dtreader.GetValue(14))
                fbcommand.Parameters.AddWithValue("@CST_PIS", dtreader.GetValue(15))
                fbcommand.Parameters.AddWithValue("@CST_COFINS", dtreader.GetValue(16))
                fbcommand.Parameters.AddWithValue("@PIS", dtreader.GetValue(17))
                fbcommand.Parameters.AddWithValue("@COFINS", dtreader.GetValue(18))
                fbcommand.Parameters.AddWithValue("@UNI_MEDIDA", dtreader.GetValue(19))
                fbcommand.Parameters.AddWithValue("@MARGEM_PV", dtreader.GetValue(20))
                fbcommand.Parameters.AddWithValue("@CFOP", dtreader.GetValue(21))
                fbcommand.Parameters.AddWithValue("@OBSERVACAO", dtreader.GetValue(22))
                fbcommand.Parameters.AddWithValue("@NAT_RECEITA", dtreader.GetValue(23))
                fbcommand.Parameters.AddWithValue("@CFOP_NF", dtreader.GetValue(24))
                fbcommand.Parameters.AddWithValue("@PRC_ATACADO", dtreader.GetValue(25))
                fbcommand.Parameters.AddWithValue("@ID_CTI_PART", dtreader.GetValue(26))
                fbcommand.Parameters.AddWithValue("@ID_CTI_FCP", dtreader.GetValue(27))
                fbcommand.Parameters.AddWithValue("@QTD_ATACADO", dtreader.GetValue(28))

                Try
                    fbcommand.Prepare()
                    fbcommand.ExecuteNonQuery()
                    fbcommand.Parameters.Clear()
                Catch ex As Exception
                    MsgBox(ex.Message, vbExclamation, "Opps... problemas na Conversão")
                    Conversor.listSaida.Items.Add("Error: " & ex.Message)
                End Try

            End If

        End While

        connOrigem.Close()

        Dim cmdCountID = New FbCommand("SELECT MAX(ID_ESTOQUE) FROM TB_ESTOQUE;", connDestino)
        Dim reader As FbDataReader = cmdCountID.ExecuteReader
        reader.Read()

        If Not IsDBNull(reader.GetValue(0)) Then
            Dim cmdUpdateId = New FbCommand("ALTER SEQUENCE GEN_TB_ESTOQUE_ID RESTART WITH " & reader.GetValue(0), connDestino)
            cmdUpdateId.Prepare()
            cmdUpdateId.ExecuteNonQuery()
        End If

        connDestino.Close()

    End Sub

    Sub Estoque_Fornecedor(dbPathOrigem, dbPathDestino)

        Dim connStrOrigem = "User=SYSDBA;Password=masterkey;Database=" & dbPathOrigem & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connOrigem As FbConnection = New FbConnection(connStrOrigem)
        connOrigem.Open()

        Dim cmdSelect = New FbCommand("SELECT 
                                            ID_IDENTIFICADOR, ID_FORNEC, COD_NO_FORNECEDOR, CST, CSOSN, COFINS, CST_COFINS, 
                                            PIS, CST_PIS, ALIQ_ICMS, UNI_MEDIDA, ID_EST_FORNEC
                                       FROM TB_ESTOQUE_FORNECEDOR;", connOrigem)

        Dim dtreader As FbDataReader = cmdSelect.ExecuteReader

        Dim connStrDestino = "User=SYSDBA;Password=masterkey;Database=" & dbPathDestino & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connDestino As FbConnection = New FbConnection(connStrDestino)

        connDestino.Open()

        Dim fbcommand = New FbCommand("INSERT INTO TB_ESTOQUE_FORNECEDOR 
                                            (ID_IDENTIFICADOR, ID_FORNEC, COD_NO_FORNECEDOR, CST, CSOSN, COFINS, CST_COFINS, 
                                             PIS, CST_PIS, ALIQ_ICMS, UNI_MEDIDA, ID_EST_FORNEC) 
                                       VALUES 
                                            (@ID_IDENTIFICADOR, @ID_FORNEC, @COD_NO_FORNECEDOR, @CST, @CSOSN, @COFINS, @CST_COFINS, 
                                             @PIS, @CST_PIS, @ALIQ_ICMS, @UNI_MEDIDA, @ID_EST_FORNEC);", connDestino)

        While (dtreader.Read)
            If (dtreader.GetValue(0) = 0) Then
                ' Verifica se é o Clientes Diversos
            Else
                fbcommand.Parameters.AddWithValue("@ID_IDENTIFICADOR", dtreader.GetValue(0))
                fbcommand.Parameters.AddWithValue("@ID_FORNEC", dtreader.GetValue(1))
                fbcommand.Parameters.AddWithValue("@COD_NO_FORNECEDOR", dtreader.GetValue(2))
                fbcommand.Parameters.AddWithValue("@CST", dtreader.GetValue(3))
                fbcommand.Parameters.AddWithValue("@CSOSN", dtreader.GetValue(4))
                fbcommand.Parameters.AddWithValue("@COFINS", dtreader.GetValue(5))
                fbcommand.Parameters.AddWithValue("@CST_COFINS", dtreader.GetValue(6))
                fbcommand.Parameters.AddWithValue("@PIS", dtreader.GetValue(7))
                fbcommand.Parameters.AddWithValue("@CST_PIS", dtreader.GetValue(8))
                fbcommand.Parameters.AddWithValue("@ALIQ_ICMS", dtreader.GetValue(9))
                fbcommand.Parameters.AddWithValue("@UNI_MEDIDA", dtreader.GetValue(10))
                fbcommand.Parameters.AddWithValue("@ID_EST_FORNEC", dtreader.GetValue(11))

                Try
                    fbcommand.Prepare()
                    fbcommand.ExecuteNonQuery()
                    fbcommand.Parameters.Clear()
                Catch ex As Exception
                    MsgBox(ex.Message, vbExclamation, "Opps... problemas na Conversão")
                    Conversor.listSaida.Items.Add("Error: " & ex.Message)
                End Try

            End If

        End While
        connOrigem.Close()
        connDestino.Close()

    End Sub

    Sub Est_Fornecedor(dbPathOrigem, dbPathDestino)

        Dim connStrOrigem = "User=SYSDBA;Password=masterkey;Database=" & dbPathOrigem & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connOrigem As FbConnection = New FbConnection(connStrOrigem)
        connOrigem.Open()

        Dim cmdSelect = New FbCommand("SELECT 
                                            ID_FORNEC, ID_ESTOQUE, PREFERENCIAL
                                        FROM TB_EST_FORNECEDOR;", connOrigem)

        Dim dtreader As FbDataReader = cmdSelect.ExecuteReader

        Dim connStrDestino = "User=SYSDBA;Password=masterkey;Database=" & dbPathDestino & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connDestino As FbConnection = New FbConnection(connStrDestino)

        connDestino.Open()

        Dim fbcommand = New FbCommand("INSERT INTO TB_EST_FORNECEDOR 
                                            (ID_FORNEC, ID_ESTOQUE, PREFERENCIAL) 
                                       VALUES 
                                            (@ID_FORNEC, @ID_ESTOQUE, @PREFERENCIAL);", connDestino)
        While (dtreader.Read)
            If (dtreader.GetValue(0) = 0) Then
                ' Verifica se é o Clientes Diversos
            Else
                fbcommand.Parameters.AddWithValue("@ID_FORNEC", dtreader.GetValue(0))
                fbcommand.Parameters.AddWithValue("@ID_ESTOQUE", dtreader.GetValue(1))
                fbcommand.Parameters.AddWithValue("@PREFERENCIAL", dtreader.GetValue(2))

                Try
                    fbcommand.Prepare()
                    fbcommand.ExecuteNonQuery()
                    fbcommand.Parameters.Clear()
                Catch ex As Exception
                    MsgBox(ex.Message, vbExclamation, "Opps... problemas na Conversão")
                    Conversor.listSaida.Items.Add("Error: " & ex.Message)
                End Try

            End If

        End While
        connOrigem.Close()
        connDestino.Close()

    End Sub

    Sub Est_Grupo(dbPathOrigem, dbPathDestino)

        Dim connStrOrigem = "User=SYSDBA;Password=masterkey;Database=" & dbPathOrigem & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connOrigem As FbConnection = New FbConnection(connStrOrigem)
        connOrigem.Open()

        Dim cmdSelect = New FbCommand("SELECT 
                                            ID_GRUPO, DESCRICAO 
                                        FROM TB_EST_GRUPO;", connOrigem)

        Dim dtreader As FbDataReader = cmdSelect.ExecuteReader

        Dim connStrDestino = "User=SYSDBA;Password=masterkey;Database=" & dbPathDestino & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connDestino As FbConnection = New FbConnection(connStrDestino)

        connDestino.Open()

        Dim fbcommand = New FbCommand("INSERT INTO TB_EST_GRUPO 
                                            (ID_GRUPO, DESCRICAO) 
                                       VALUES 
                                            (@ID_GRUPO, @DESCRICAO);", connDestino)
        While (dtreader.Read)
            If (dtreader.GetValue(0) = 0) Then
                ' Verifica se é o Clientes Diversos
            Else
                fbcommand.Parameters.AddWithValue("@ID_GRUPO", dtreader.GetValue(0))
                fbcommand.Parameters.AddWithValue("@DESCRICAO", dtreader.GetValue(1))

                Try
                    fbcommand.Prepare()
                    fbcommand.ExecuteNonQuery()
                    fbcommand.Parameters.Clear()
                Catch ex As Exception
                    MsgBox(ex.Message, vbExclamation, "Opps... problemas na Conversão")
                    Conversor.listSaida.Items.Add("Error: " & ex.Message)
                End Try

            End If

        End While

        connOrigem.Close()

        ' # TEM QUE TRATAR GRUPOS NULLOS

        Dim cmdCountID = New FbCommand("SELECT MAX(ID_GRUPO) FROM TB_EST_GRUPO;", connDestino)
        Dim reader As FbDataReader = cmdCountID.ExecuteReader
        reader.Read()
        If IsDBNull(reader.GetValue(0)) = False Then
            Dim cmdUpdateId = New FbCommand("ALTER SEQUENCE GEN_TB_EST_GRUPO_ID RESTART WITH " & reader.GetValue(0), connDestino)
            cmdUpdateId.Prepare()
            cmdUpdateId.ExecuteNonQuery()
        End If


        connDestino.Close()

    End Sub

    Sub Est_Identificador(dbPathOrigem, dbPathDestino)

        Dim connStrOrigem = "User=SYSDBA;Password=masterkey;Database=" & dbPathOrigem & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connOrigem As FbConnection = New FbConnection(connStrOrigem)
        connOrigem.Open()

        Dim cmdSelect = New FbCommand("SELECT
                                            ID_IDENTIFICADOR, ID_ESTOQUE, CHAVE
                                       FROM TB_EST_IDENTIFICADOR;", connOrigem)

        Dim dtreader As FbDataReader = cmdSelect.ExecuteReader

        Dim connStrDestino = "User=SYSDBA;Password=masterkey;Database=" & dbPathDestino & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connDestino As FbConnection = New FbConnection(connStrDestino)

        connDestino.Open()

        Dim fbcommand = New FbCommand("INSERT INTO TB_EST_IDENTIFICADOR 
                                            (ID_IDENTIFICADOR, ID_ESTOQUE, CHAVE) 
                                        VALUES 
                                            (@ID_IDENTIFICADOR, @ID_ESTOQUE, @CHAVE);", connDestino)
        While (dtreader.Read)
            If (dtreader.GetValue(0) = 0) Then
                ' Verifica se é o Clientes Diversos
            Else
                fbcommand.Parameters.AddWithValue("@ID_IDENTIFICADOR", dtreader.GetValue(0))
                fbcommand.Parameters.AddWithValue("@ID_ESTOQUE", dtreader.GetValue(1))
                fbcommand.Parameters.AddWithValue("@CHAVE", dtreader.GetValue(2))

                Try
                    fbcommand.Prepare()
                    fbcommand.ExecuteNonQuery()
                    fbcommand.Parameters.Clear()
                Catch ex As Exception
                    MsgBox(ex.Message, vbExclamation, "Opps... problemas na Conversão")
                    Conversor.listSaida.Items.Add("Error: " & ex.Message)
                End Try

            End If

        End While

        connOrigem.Close()

        Dim cmdCountID = New FbCommand("SELECT MAX(ID_IDENTIFICADOR) FROM TB_EST_IDENTIFICADOR", connDestino)
        Dim reader As FbDataReader = cmdCountID.ExecuteReader
        reader.Read()

        If Not IsDBNull(reader.GetValue(0)) Then
            Dim cmdUpdateId = New FbCommand("ALTER SEQUENCE GEN_TB_EST_IDENTIFICADOR_ID RESTART WITH " & reader.GetValue(0), connDestino)
            cmdUpdateId.Prepare()
            cmdUpdateId.ExecuteNonQuery()
        End If


        connDestino.Close()

    End Sub
    Sub Est_Produto(dbPathOrigem, dbPathDestino)

        Dim connStrOrigem = "User=SYSDBA;Password=masterkey;Database=" & dbPathOrigem & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connOrigem As FbConnection = New FbConnection(connStrOrigem)
        connOrigem.Open()

        Dim cmdSelect = New FbCommand("SELECT 
                                            ID_IDENTIFICADOR, DESC_CMPL, COD_BARRA, REFERENCIA, PRC_MEDIO, QTD_COMPRA, 
                                            QTD_ATUAL, QTD_MINIM, QTD_INICIO, QTD_RESERV, QTD_POSVEN, ULT_COMPRA, PESO, 
                                            IPI, CF, IAT, IPPT, COD_NCM, ID_NIVEL1, ID_NIVEL2, MVA, CST_IPI, FOTO, CSOSN, 
                                            ANP, EXTIPI, CST, FCI, COD_CEST, CENQ, VLR_IPI, CST_CFE, CSOSN_CFE, 
                                            CONTROLA_LOTE_VENDA, BAIXA_LOTE_NFV, BAIXA_LOTE_PDV, IND_ESCALA, CNPJ_FABRICANTE
FROM TB_EST_PRODUTO;
;", connOrigem)

        Dim dtreader As FbDataReader = cmdSelect.ExecuteReader

        Dim connStrDestino = "User=SYSDBA;Password=masterkey;Database=" & dbPathDestino & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connDestino As FbConnection = New FbConnection(connStrDestino)

        connDestino.Open()

        Dim fbcommand = New FbCommand("INSERT INTO TB_EST_PRODUTO 
                                            (ID_IDENTIFICADOR, DESC_CMPL, COD_BARRA, REFERENCIA, PRC_MEDIO, QTD_COMPRA, 
                                            QTD_ATUAL, QTD_MINIM, QTD_INICIO, QTD_RESERV, QTD_POSVEN, ULT_COMPRA, PESO, 
                                            IPI, CF, IAT, IPPT, COD_NCM, ID_NIVEL1, ID_NIVEL2, MVA, CST_IPI, FOTO, CSOSN, 
                                            ANP, EXTIPI, CST, FCI, COD_CEST, CENQ, VLR_IPI, CST_CFE, CSOSN_CFE, 
                                            CONTROLA_LOTE_VENDA, BAIXA_LOTE_NFV, BAIXA_LOTE_PDV, IND_ESCALA, CNPJ_FABRICANTE) 
                                    VALUES 
                                            (@ID_IDENTIFICADOR, @DESC_CMPL, @COD_BARRA, @REFERENCIA, @PRC_MEDIO, @QTD_COMPRA, 
                                            @QTD_ATUAL, @QTD_MINIM, @QTD_INICIO, @QTD_RESERV, @QTD_POSVEN, @ULT_COMPRA, @PESO, 
                                            @IPI, @CF, @IAT, @IPPT, @COD_NCM, @ID_NIVEL1, @ID_NIVEL2, @MVA, @CST_IPI, @FOTO, @CSOSN, 
                                            @ANP, @EXTIPI, @CST, @FCI, @COD_CEST, @CENQ, @VLR_IPI, @CST_CFE, @CSOSN_CFE,
                                            @CONTROLA_LOTE_VENDA, @BAIXA_LOTE_NFV, @BAIXA_LOTE_PDV, @IND_ESCALA, @CNPJ_FABRICANTE);", connDestino)

        While (dtreader.Read)
            If (dtreader.GetValue(0) = 0) Then
                ' Verifica se é o Clientes Diversos
            Else
                fbcommand.Parameters.AddWithValue("@ID_IDENTIFICADOR", dtreader.GetValue(0))
                fbcommand.Parameters.AddWithValue("@DESC_CMPL", dtreader.GetValue(1))
                fbcommand.Parameters.AddWithValue("@COD_BARRA", dtreader.GetValue(2))
                fbcommand.Parameters.AddWithValue("@REFERENCIA", dtreader.GetValue(3))
                fbcommand.Parameters.AddWithValue("@PRC_MEDIO", dtreader.GetValue(4))
                fbcommand.Parameters.AddWithValue("@QTD_COMPRA", dtreader.GetValue(5))
                fbcommand.Parameters.AddWithValue("@QTD_ATUAL", dtreader.GetValue(6))
                fbcommand.Parameters.AddWithValue("@QTD_MINIM", dtreader.GetValue(7))
                fbcommand.Parameters.AddWithValue("@QTD_INICIO", dtreader.GetValue(8))
                fbcommand.Parameters.AddWithValue("@QTD_RESERV", dtreader.GetValue(9))
                fbcommand.Parameters.AddWithValue("@QTD_POSVEN", dtreader.GetValue(10))
                fbcommand.Parameters.AddWithValue("@ULT_COMPRA", dtreader.GetValue(11))
                fbcommand.Parameters.AddWithValue("@PESO", dtreader.GetValue(12))
                fbcommand.Parameters.AddWithValue("@IPI", dtreader.GetValue(13))
                fbcommand.Parameters.AddWithValue("@CF", dtreader.GetValue(14))
                fbcommand.Parameters.AddWithValue("@IAT", dtreader.GetValue(15))
                fbcommand.Parameters.AddWithValue("@IPPT", dtreader.GetValue(16))
                fbcommand.Parameters.AddWithValue("@COD_NCM", dtreader.GetValue(17))
                fbcommand.Parameters.AddWithValue("@ID_NIVEL1", dtreader.GetValue(18))
                fbcommand.Parameters.AddWithValue("@ID_NIVEL2", dtreader.GetValue(19))
                fbcommand.Parameters.AddWithValue("@MVA", dtreader.GetValue(20))
                fbcommand.Parameters.AddWithValue("@CST_IPI", dtreader.GetValue(21))
                fbcommand.Parameters.AddWithValue("@FOTO", dtreader.GetValue(22))
                fbcommand.Parameters.AddWithValue("@CSOSN", dtreader.GetValue(23))
                fbcommand.Parameters.AddWithValue("@ANP", dtreader.GetValue(24))
                fbcommand.Parameters.AddWithValue("@EXTIPI", dtreader.GetValue(25))
                fbcommand.Parameters.AddWithValue("@CST", dtreader.GetValue(26))
                fbcommand.Parameters.AddWithValue("@FCI", dtreader.GetValue(27))
                fbcommand.Parameters.AddWithValue("@COD_CEST", dtreader.GetValue(28))
                fbcommand.Parameters.AddWithValue("@CENQ", dtreader.GetValue(29))
                fbcommand.Parameters.AddWithValue("@VLR_IPI", dtreader.GetValue(30))
                fbcommand.Parameters.AddWithValue("@CST_CFE", dtreader.GetValue(31))
                fbcommand.Parameters.AddWithValue("@CSOSN_CFE", dtreader.GetValue(32))
                fbcommand.Parameters.AddWithValue("@CONTROLA_LOTE_VENDA", dtreader.GetValue(33))
                fbcommand.Parameters.AddWithValue("@BAIXA_LOTE_NFV", dtreader.GetValue(34))
                fbcommand.Parameters.AddWithValue("@BAIXA_LOTE_PDV", dtreader.GetValue(35))
                fbcommand.Parameters.AddWithValue("@IND_ESCALA", dtreader.GetValue(36))
                fbcommand.Parameters.AddWithValue("@CNPJ_FABRICANTE", dtreader.GetValue(37))

                Try
                    fbcommand.Prepare()
                    fbcommand.ExecuteNonQuery()
                    fbcommand.Parameters.Clear()
                Catch ex As Exception
                    MsgBox(ex.Message, vbExclamation, "Opps... problemas na Conversão")
                    Conversor.listSaida.Items.Add("Error: " & ex.Message)
                End Try

            End If

        End While
        connOrigem.Close()
        connDestino.Close()

    End Sub

    Sub Est_Prod_Nivel_1(dbPathOrigem, dbPathDestino)

        Dim connStrOrigem = "User=SYSDBA;Password=masterkey;Database=" & dbPathOrigem & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connOrigem As FbConnection = New FbConnection(connStrOrigem)
        connOrigem.Open()

        Dim cmdSelect = New FbCommand("select 
                                            id_nivel1, descricao
                                       from tb_est_prod_nivel1;", connOrigem)

        Dim dtreader As FbDataReader = cmdSelect.ExecuteReader

        Dim connStrDestino = "User=SYSDBA;Password=masterkey;Database=" & dbPathDestino & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connDestino As FbConnection = New FbConnection(connStrDestino)

        connDestino.Open()

        Dim fbcommand = New FbCommand("INSERT INTO 
                                            TB_EST_PROD_NIVEL1 (ID_NIVEL1, DESCRICAO) 
                                       VALUES 
                                            (@ID_NIVEL1, @DESCRICAO);", connDestino)

        While (dtreader.Read)
            If (dtreader.GetValue(0) = 0) Then
                ' Verifica se é o Clientes Diversos
            Else
                fbcommand.Parameters.AddWithValue("@ID_NIVEL1", dtreader.GetValue(0))
                fbcommand.Parameters.AddWithValue("@DESCRICAO", dtreader.GetValue(1))
        
                Try
                    fbcommand.Prepare()
                    fbcommand.ExecuteNonQuery()
                    fbcommand.Parameters.Clear()
                Catch ex As Exception
                    MsgBox(ex.Message, vbExclamation, "Opps... problemas na Conversão")
                    Conversor.listSaida.Items.Add("Error: " & ex.Message)
                End Try

            End If

        End While

        connOrigem.Close()

        Dim cmdCountID = New FbCommand("SELECT MAX(ID_NIVEL1) FROM TB_EST_PROD_NIVEL1;", connDestino)
        Dim reader As FbDataReader = cmdCountID.ExecuteReader
        reader.Read()

        If Not IsDBNull(reader.GetValue(0)) Then

            Dim cmdUpdateId = New FbCommand("ALTER SEQUENCE GEN_TB_EST_PROD_NIVEL1_ID RESTART WITH " & reader.GetValue(0), connDestino)
            cmdUpdateId.Prepare()
            cmdUpdateId.ExecuteNonQuery()

        End If


        connDestino.Close()

    End Sub

        Sub Est_Prod_Nivel_2(dbPathOrigem, dbPathDestino)

        Dim connStrOrigem = "User=SYSDBA;Password=masterkey;Database=" & dbPathOrigem & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connOrigem As FbConnection = New FbConnection(connStrOrigem)
        connOrigem.Open()

        Dim cmdSelect = New FbCommand("select 
                                            id_nivel1, descricao
                                       from tb_est_prod_nivel1;", connOrigem)

        Dim dtreader As FbDataReader = cmdSelect.ExecuteReader

        Dim connStrDestino = "User=SYSDBA;Password=masterkey;Database=" & dbPathDestino & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connDestino As FbConnection = New FbConnection(connStrDestino)

        connDestino.Open()

        Dim fbcommand = New FbCommand("INSERT INTO 
                                            TB_EST_PROD_NIVEL2 (ID_NIVEL2, DESCRICAO) 
                                       VALUES 
                                            (@ID_NIVEL2, @DESCRICAO);", connDestino)
        While (dtreader.Read)
            If (dtreader.GetValue(0) = 0) Then
                ' Verifica se é o Clientes Diversos
            Else
                fbcommand.Parameters.AddWithValue("@ID_NIVEL2", dtreader.GetValue(0))
                fbcommand.Parameters.AddWithValue("@DESCRICAO", dtreader.GetValue(1))

                Try
                    fbcommand.Prepare()
                    fbcommand.ExecuteNonQuery()
                    fbcommand.Parameters.Clear()
                Catch ex As Exception
                    MsgBox(ex.Message, vbExclamation, "Opps... problemas na Conversão")
                    Conversor.listSaida.Items.Add("Error: " & ex.Message)
                End Try

            End If

        End While


        connOrigem.Close()

        Dim cmdCountID = New FbCommand("SELECT MAX(ID_NIVEL2) FROM TB_EST_PROD_NIVEL2;", connDestino)
        Dim reader As FbDataReader = cmdCountID.ExecuteReader
        reader.Read()

        If Not IsDBNull(reader.GetValue(0)) Then


            Dim cmdUpdateId = New FbCommand("ALTER SEQUENCE GEN_TB_EST_PROD_NIVEL2_ID RESTART WITH " & reader.GetValue(0), connDestino)
            cmdUpdateId.Prepare()
            cmdUpdateId.ExecuteNonQuery()
        End If


        connDestino.Close()

    End Sub

    Sub Fornecedor(dbPathOrigem, dbPathDestino)

        Dim connStrOrigem = "User=SYSDBA;Password=masterkey;Database=" & dbPathOrigem & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connOrigem As FbConnection = New FbConnection(connStrOrigem)
        connOrigem.Open()

        Dim cmdSelect = New FbCommand("select 
                                            id_fornec, nome, nome_fanta, cnpj, insc_estad, insc_munic,
                                            end_cep, end_tipo, end_lograd, end_bairro, end_numero,
                                            end_comple, ddd_comer, fone_comer, fone_0800, ddd_celul,
                                            fone_celul, ddd_fax, fone_fax, email_cont, email_nfe,
                                            site, status, dt_pricomp, dt_ultcomp, id_cidade, limite,
                                            id_ramo, id_pais, observacao, contato
                                        from tb_fornecedor;", connOrigem)

        Dim dtreader As FbDataReader = cmdSelect.ExecuteReader


        Dim connStrDestino = "User=SYSDBA;Password=masterkey;Database=" & dbPathDestino & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connDestino As FbConnection = New FbConnection(connStrDestino)

        connDestino.Open()

        Dim fbcommand = New FbCommand("INSERT INTO TB_FORNECEDOR 
                                                                (ID_FORNEC, NOME, NOME_FANTA, CNPJ, INSC_ESTAD,
                                                                 INSC_MUNIC, END_CEP, END_TIPO, END_LOGRAD, 
                                                                 END_BAIRRO, END_NUMERO, END_COMPLE, DDD_COMER, 
                                                                 FONE_COMER, FONE_0800, DDD_CELUL, FONE_CELUL,
                                                                 DDD_FAX, FONE_FAX, EMAIL_CONT, EMAIL_NFE, SITE,
                                                                 STATUS, DT_PRICOMP, DT_ULTCOMP, ID_CIDADE, LIMITE,
                                                                 ID_RAMO, ID_PAIS, OBSERVACAO, CONTATO) 
                                                            VALUES
                                                                (@ID_FORNEC, @NOME, @NOME_FANTA, @CNPJ, @INSC_ESTAD,
                                                                 @INSC_MUNIC, @END_CEP, @END_TIPO, @END_LOGRAD,
                                                                 @END_BAIRRO, @END_NUMERO, @END_COMPLE, @DDD_COMER,
                                                                 @FONE_COMER, @FONE_0800, @DDD_CELUL, @FONE_CELUL, 
                                                                 @DDD_FAX, @FONE_FAX, @EMAIL_CONT, @EMAIL_NFE, @SITE,
                                                                 @STATUS, @DT_PRICOMP, @DT_ULTCOMP, @ID_CIDADE, @LIMITE,
                                                                 @ID_RAMO, @ID_PAIS, @OBSERVACAO, @CONTATO);", connDestino)


        While (dtreader.Read)
            If (dtreader.GetValue(0) = 0) Then
                ' Verifica se é o Clientes Diversos
            Else
                fbcommand.Parameters.AddWithValue("@ID_FORNEC", dtreader.GetValue(0))
                fbcommand.Parameters.AddWithValue("@NOME", dtreader.GetValue(1))
                fbcommand.Parameters.AddWithValue("@NOME_FANTA", dtreader.GetValue(2))
                fbcommand.Parameters.AddWithValue("@CNPJ", dtreader.GetValue(3))
                fbcommand.Parameters.AddWithValue("@INSC_ESTAD", dtreader.GetValue(4))
                fbcommand.Parameters.AddWithValue("@INSC_MUNIC", dtreader.GetValue(5))
                fbcommand.Parameters.AddWithValue("@END_CEP", dtreader.GetValue(6))
                fbcommand.Parameters.AddWithValue("@END_TIPO", dtreader.GetValue(7))
                fbcommand.Parameters.AddWithValue("@END_LOGRAD", dtreader.GetValue(8))
                fbcommand.Parameters.AddWithValue("@END_BAIRRO", dtreader.GetValue(9))
                fbcommand.Parameters.AddWithValue("@END_NUMERO", dtreader.GetValue(10))
                fbcommand.Parameters.AddWithValue("@END_COMPLE", dtreader.GetValue(11))
                fbcommand.Parameters.AddWithValue("@DDD_COMER", dtreader.GetValue(12))
                fbcommand.Parameters.AddWithValue("@FONE_COMER", dtreader.GetValue(13))
                fbcommand.Parameters.AddWithValue("@FONE_0800", dtreader.GetValue(14))
                fbcommand.Parameters.AddWithValue("@DDD_CELUL", dtreader.GetValue(15))
                fbcommand.Parameters.AddWithValue("@FONE_CELUL", dtreader.GetValue(16))
                fbcommand.Parameters.AddWithValue("@DDD_FAX", dtreader.GetValue(17))
                fbcommand.Parameters.AddWithValue("@FONE_FAX", dtreader.GetValue(18))
                fbcommand.Parameters.AddWithValue("@EMAIL_CONT", dtreader.GetValue(19))
                fbcommand.Parameters.AddWithValue("@EMAIL_NFE", dtreader.GetValue(20))
                fbcommand.Parameters.AddWithValue("@SITE", dtreader.GetValue(21))
                fbcommand.Parameters.AddWithValue("@STATUS", dtreader.GetValue(22))
                fbcommand.Parameters.AddWithValue("@DT_PRICOMP", dtreader.GetValue(23))
                fbcommand.Parameters.AddWithValue("@DT_ULTCOMP", dtreader.GetValue(24))
                fbcommand.Parameters.AddWithValue("@ID_CIDADE", dtreader.GetValue(25))
                fbcommand.Parameters.AddWithValue("@LIMITE", dtreader.GetValue(26))
                fbcommand.Parameters.AddWithValue("@ID_RAMO", dtreader.GetValue(27))
                fbcommand.Parameters.AddWithValue("@ID_PAIS", dtreader.GetValue(28))
                fbcommand.Parameters.AddWithValue("@OBSERVACAO", dtreader.GetValue(29))
                fbcommand.Parameters.AddWithValue("@CONTATO", dtreader.GetValue(30))
        
                Try
                    fbcommand.Prepare()
                    fbcommand.ExecuteNonQuery()
                    fbcommand.Parameters.Clear()
                Catch ex As Exception
                    MsgBox(ex.Message, vbExclamation, "Opps... problemas na Conversão")
                    Conversor.listSaida.Items.Add("Error: " & ex.Message)
                End Try

            End If

        End While

        connOrigem.Close()

        Dim cmdCountID = New FbCommand("SELECT MAX(ID_FORNEC) FROM TB_FORNECEDOR;", connDestino)
        Dim reader As FbDataReader = cmdCountID.ExecuteReader
        reader.Read()

        If Not IsDBNull(reader.GetValue(0)) Then
            Dim cmdUpdateId = New FbCommand("ALTER SEQUENCE GEN_TB_FORNEC_ID RESTART WITH " & reader.GetValue(0), connDestino)
            cmdUpdateId.Prepare()
            cmdUpdateId.ExecuteNonQuery()
        End If

        connDestino.Close()

    End Sub

    Sub Taxa_UF(dbPathOrigem, dbPathDestino)

        Dim connStrOrigem = "User=SYSDBA;Password=masterkey;Database=" & dbPathOrigem & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connOrigem As FbConnection = New FbConnection(connStrOrigem)
        connOrigem.Open()

        Dim cmdSelect = New FbCommand("select 
                                            id_cti, descricao, base_icms, base_icmsfe,
                                            base_icms_st, uf_ac, uf_al, uf_am, uf_ap, uf_ba, uf_ce, uf_df,
                                            uf_es, uf_go, uf_ma, uf_mg, uf_ms, uf_mt, uf_pa, uf_pb,
                                            uf_pe, uf_pi, uf_pr, uf_rj, uf_rn, uf_ro, uf_rr, uf_rs,
                                            uf_sc, uf_se, uf_sp, uf_to, base_iss, iss, por_dif
                                        from tb_taxa_uf", connOrigem)

        Dim dtreader As FbDataReader = cmdSelect.ExecuteReader

        Dim connStrDestino = "User=SYSDBA;Password=masterkey;Database=" & dbPathDestino & ";DataSource=localhost;Port=3050;Dialect=3;"

        Dim connDestino As FbConnection = New FbConnection(connStrDestino)

        connDestino.Open()

        Dim delTaxas = New FbCommand("DELETE FROM TB_TAXA_UF", connDestino)
        Try
            delTaxas.Prepare()
            delTaxas.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Dim fbcommand = New FbCommand("INSERT INTO TB_TAXA_UF 
                                                                (ID_CTI, DESCRICAO, BASE_ICMS, BASE_ICMSFE,
                                                                 BASE_ICMS_ST, UF_AC, UF_AL, UF_AM, UF_AP, UF_BA,
                                                                 UF_CE, UF_DF, UF_ES, UF_GO, UF_MA, UF_MG, UF_MS,
                                                                 UF_MT, UF_PA, UF_PB, UF_PE, UF_PI, UF_PR, UF_RJ,
                                                                 UF_RN, UF_RO, UF_RR, UF_RS, UF_SC, UF_SE, UF_SP,
                                                                 UF_TO, BASE_ISS, ISS, POR_DIF) 
                                                            VALUES 
                                                                (@ID_CTI, @DESCRICAO, @BASE_ICMS, @BASE_ICMSFE,
                                                                @BASE_ICMS_ST, @UF_AC, @UF_AL, @UF_AM, @UF_AP,
                                                                @UF_BA, @UF_CE, @UF_DF, @UF_ES, @UF_GO,   
                                                                @UF_MA, @UF_MG, @UF_MS, @UF_MT, @UF_PA, @UF_PB,
                                                                @UF_PE, @UF_PI, @UF_PR, @UF_RJ,  @UF_RN, @UF_RO,
                                                                @UF_RR, @UF_RS, @UF_SC, @UF_SE, @UF_SP,  @UF_TO,
                                                                @BASE_ISS, @ISS, @POR_DIF) ;", connDestino)

        While (dtreader.Read)
            fbcommand.Parameters.AddWithValue("@ID_CTI", dtreader.GetValue(0))
            fbcommand.Parameters.AddWithValue("@DESCRICAO", dtreader.GetValue(1))
                fbcommand.Parameters.AddWithValue("@BASE_ICMS", dtreader.GetValue(2))
                fbcommand.Parameters.AddWithValue("@BASE_ICMSFE", dtreader.GetValue(3))
                fbcommand.Parameters.AddWithValue("@BASE_ICMS_ST", dtreader.GetValue(4))
                fbcommand.Parameters.AddWithValue("@UF_AC", dtreader.GetValue(5))
                fbcommand.Parameters.AddWithValue("@UF_AL", dtreader.GetValue(6))
                fbcommand.Parameters.AddWithValue("@UF_AM", dtreader.GetValue(7))
                fbcommand.Parameters.AddWithValue("@UF_AP", dtreader.GetValue(8))
                fbcommand.Parameters.AddWithValue("@UF_BA", dtreader.GetValue(9))
                fbcommand.Parameters.AddWithValue("@UF_CE", dtreader.GetValue(10))
                fbcommand.Parameters.AddWithValue("@UF_DF", dtreader.GetValue(11))
                fbcommand.Parameters.AddWithValue("@UF_ES", dtreader.GetValue(12))
                fbcommand.Parameters.AddWithValue("@UF_GO", dtreader.GetValue(13))
                fbcommand.Parameters.AddWithValue("@UF_MA", dtreader.GetValue(14))
                fbcommand.Parameters.AddWithValue("@UF_MG", dtreader.GetValue(15))
                fbcommand.Parameters.AddWithValue("@UF_MS", dtreader.GetValue(16))
                fbcommand.Parameters.AddWithValue("@UF_MT", dtreader.GetValue(17))
                fbcommand.Parameters.AddWithValue("@UF_PA", dtreader.GetValue(18))
                fbcommand.Parameters.AddWithValue("@UF_PB", dtreader.GetValue(19))
                fbcommand.Parameters.AddWithValue("@UF_PE", dtreader.GetValue(20))
                fbcommand.Parameters.AddWithValue("@UF_PI", dtreader.GetValue(21))
                fbcommand.Parameters.AddWithValue("@UF_PR", dtreader.GetValue(22))
                fbcommand.Parameters.AddWithValue("@UF_RJ", dtreader.GetValue(23))
                fbcommand.Parameters.AddWithValue("@UF_RN", dtreader.GetValue(24))
                fbcommand.Parameters.AddWithValue("@UF_RO", dtreader.GetValue(25))
                fbcommand.Parameters.AddWithValue("@UF_RR", dtreader.GetValue(26))
                fbcommand.Parameters.AddWithValue("@UF_RS", dtreader.GetValue(27))
                fbcommand.Parameters.AddWithValue("@UF_SC", dtreader.GetValue(28))
                fbcommand.Parameters.AddWithValue("@UF_SE", dtreader.GetValue(29))
                fbcommand.Parameters.AddWithValue("@UF_SP", dtreader.GetValue(30))
                fbcommand.Parameters.AddWithValue("@UF_TO", dtreader.GetValue(31))
                fbcommand.Parameters.AddWithValue("@BASE_ISS", dtreader.GetValue(32))
                fbcommand.Parameters.AddWithValue("@ISS", dtreader.GetValue(33))
                fbcommand.Parameters.AddWithValue("@POR_DIF", dtreader.GetValue(34))

            Try
                fbcommand.Prepare()
                fbcommand.ExecuteNonQuery()
                fbcommand.Parameters.Clear()
            Catch ex As Exception
                MsgBox(ex.Message, vbExclamation, "Opps... problemas na Conversão")
                Conversor.listSaida.Items.Add("Error: " & ex.Message)
            End Try

        End While
        connOrigem.Close()
        connDestino.Close()

    End Sub

    Sub Uni_Medida(dbPathOrigem, dbPathDestino)

        Dim connStrOrigem = "User=SYSDBA;Password=masterkey;Database=" & dbPathOrigem & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connOrigem As FbConnection = New FbConnection(connStrOrigem)
        connOrigem.Open()

        Dim cmdSelect = New FbCommand("SELECT 
                                             UNIDADE, DESCRICAO, CONVERSOR, STATUS, UNIDADE_EX
                                        FROM TB_UNI_MEDIDA;", connOrigem)

        Dim dtreader As FbDataReader = cmdSelect.ExecuteReader

        Dim connStrDestino = "User=SYSDBA;Password=masterkey;Database=" & dbPathDestino & ";DataSource=localhost;Port=3050;Dialect=3;"
        Dim connDestino As FbConnection = New FbConnection(connStrDestino)

        connDestino.Open()

        Dim delData = New FbCommand("DELETE FROM TB_UNI_MEDIDA", connDestino)
        Try
            delData.Prepare()
            delData.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Dim fbcommand = New FbCommand("INSERT INTO TB_UNI_MEDIDA 
                                            (UNIDADE, DESCRICAO, CONVERSOR, STATUS, UNIDADE_EX) 
                                       VALUES 
                                            (@UNIDADE, @DESCRICAO, @CONVERSOR, @STATUS, @UNIDADE_EX);", connDestino)
        While (dtreader.Read)
            fbcommand.Parameters.AddWithValue("@UNIDADE", dtreader.GetValue(0))
                fbcommand.Parameters.AddWithValue("@DESCRICAO", dtreader.GetValue(1))
                fbcommand.Parameters.AddWithValue("@CONVERSOR", dtreader.GetValue(2))
                fbcommand.Parameters.AddWithValue("@STATUS", dtreader.GetValue(3))
                fbcommand.Parameters.AddWithValue("@UNIDADE_EX", dtreader.GetValue(4))

            Try
                fbcommand.Prepare()
                fbcommand.ExecuteNonQuery()
                fbcommand.Parameters.Clear()
            Catch ex As Exception
                MsgBox(ex.Message, vbExclamation, "Opps... problemas na Conversão")
                Conversor.listSaida.Items.Add("Error: " & ex.Message)
            End Try

        End While
        connOrigem.Close()
        connDestino.Close()

    End Sub




End Class
