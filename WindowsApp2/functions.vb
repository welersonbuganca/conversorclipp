﻿Module functions

    Function DateFormat(data)

        data = Replace(data, "/", ".")

        Return data
    End Function

    Function GetDatabase(Txt As TextBox)
        Dim ofd = New OpenFileDialog With
            {
                .Filter = "Firebird Database|*.FDB|All Files|*.*"
            }

        Try
            ofd.ShowDialog()
            Txt.Text = ofd.FileName
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return Txt

    End Function

    Sub Progress()
        Conversor.progressConversao.Style = ProgressBarStyle.Marquee
    End Sub

End Module
