Imports System.Threading
Imports System.ComponentModel


Public Class Conversor

    Delegate Sub TesteThreadDelegate()
    Public trd As System.Threading.Thread = New Threading.Thread(AddressOf TesteThread)

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnConverter.Click


        Button1.Enabled = False
        Button2.Enabled = False
        btnConverter.Enabled = False

        progressConversao.Style = ProgressBarStyle.Marquee


        Dim pConversao As New Thread(AddressOf ProcessoConversao)

        pConversao.Start()


        'progressConversao.Style = ProgressBarStyle.Blocks

        'btnConverter.Enabled = True

    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        getDatabase(txtDataOrigem)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        getDatabase(txtDataSaida)
    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        trd.Start()


    End Sub

    Sub ProcessoConversao()



        Dim f As New FB


        If txtDataOrigem.Text = "" Then
            MsgBox("A base de dados do Cliente deve ser selecionada.")

        ElseIf txtDataSaida.Text = "" Then
            MsgBox("A base de dados para convers�o deve ser selecionada")

        ElseIf (checkClientes.Checked = False) And (checkFornecedor.Checked = False) And (checkEstoque.Checked = False) Then
            MsgBox("Pelo menos um dos M�dulos (Clientes, Fornecedores ou Estoque) deve ser selecionado!", vbExclamation, "Informa��o")



        Else

            If checkClientes.Checked = True Then
                'SetListSaida("Convertendo tabela de Clientes ..........")
                f.Clientes(txtDataOrigem.Text, txtDataSaida.Text)
                'SetListSaida("TB_CLIENTES ... OK!")
                'SetListSaida("Convertendo tabela de Pessoa F�sica ..........")
                f.Cli_PF(txtDataOrigem.Text, txtDataSaida.Text)
                'SetListSaida("TB_PF ... OK!")
                'SetListSaida("Convertendo tabela de Adicionas de PF ..........")
                f.Cli_PF_Adicionais(txtDataOrigem.Text, txtDataSaida.Text)
                'SetListSaida("TB_CLI_PF_ADICIONAIS ... OK!")
                'SetListSaida("Convertendo tabela de Pessoa Jur�dica ..........")
                f.Cli_PJ(txtDataOrigem.Text, txtDataSaida.Text)
                'SetListSaida("TB_PJ ... OK!")

            End If

            If checkFornecedor.Checked = True Then
                'SetListSaida("Convertendo tabela de Fornecedores ..........")
                f.Fornecedor(txtDataOrigem.Text, txtDataSaida.Text)
                'SetListSaida("TB_FORNECEDORES ... OK!")
            End If

            If checkEstoque.Checked = True Then
                'SetListSaida("Convertendo tabela de Unidade de Medida ..........")
                f.Uni_Medida(txtDataOrigem.Text, txtDataSaida.Text)
                'SetListSaida("TB_UNI_MEDIDA ... OK!")
                'SetListSaida("Convertendo tabela de Estoque ..........")
                f.Estoque(txtDataOrigem.Text, txtDataSaida.Text)
                'SetListSaida("TB_ESTOQUE ... OK!")
                'SetListSaida("Convertendo tabela Grupos ..........")
                f.Est_Grupo(txtDataOrigem.Text, txtDataSaida.Text)
                'SetListSaida("TB_EST_GRUPO ... OK!")
                'SetListSaida("Convertendo Estoque x Identificador ........")
                f.Est_Identificador(txtDataOrigem.Text, txtDataSaida.Text)
                'SetListSaida("TB_EST_IDENTIFICADOR ... OK!")
                'SetListSaida("Convertendo Est_Produto ........")
                f.Est_Produto(txtDataOrigem.Text, txtDataSaida.Text)
                'SetListSaida("TB_EST_PRODUTO ... OK!")
                'SetListSaida("Convertendo Est_Prod_Nivel_1")
                f.Est_Prod_Nivel_1(txtDataOrigem.Text, txtDataSaida.Text)
                'SetListSaida("TB_EST_PROD_NIVEL_1 ... OK!")
                'SetListSaida("Convertendo Est_Prod_Nivel_2")
                f.Est_Prod_Nivel_2(txtDataOrigem.Text, txtDataSaida.Text)
                'SetListSaida("TB_EST_PROD_NIVEL_2 ... OK!")

            End If
            If (checkFornecedor.Checked = True) And (checkFornecedor.Checked = True) Then

                'SetListSaida("Convertendo tabela de Estoque x Fornecedor ..........")
                f.Estoque_Fornecedor(txtDataOrigem.Text, txtDataSaida.Text)
                'SetListSaida("TB_FORNECEDOR ... OK!")
                'SetListSaida("Convertendo Est_Fornecedor ........")
                f.Est_Fornecedor(txtDataOrigem.Text, txtDataSaida.Text)
                'SetListSaida("TB_EST_FORNECEDOR ... OK!")
            End If

            ' precisa ver se vai mesmo converter isso, devido a chave estrangeira de naturezas de opera��o
            'f.Taxa_UF(txtDataOrigem.Text, txtDataSaida.Text)

            'SetListSaida("Conclu�do.")
            trd.Start()


            MsgBox("Concluido!")


        End If

        If btnConverter.Enabled = True Then
            trd.Abort()
        End If

    End Sub

    Public Function SetListSaida(text)

        Return Me.listSaida.Items.Add(text)

    End Function

    Sub MudaValor()
        progressConversao.Style = ProgressBarStyle.Blocks
        Button1.Enabled = True
        Button2.Enabled = True
        btnConverter.Enabled = True

    End Sub
    Public Sub TesteThread()
        Dim setm As TesteThreadDelegate = New TesteThreadDelegate(AddressOf mudaValor)
        progressConversao.Invoke(setm)

    End Sub



End Class
