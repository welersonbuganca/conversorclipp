﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Conversor
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Conversor))
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txtDataOrigem = New System.Windows.Forms.TextBox()
        Me.btnConverter = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.txtDataSaida = New System.Windows.Forms.TextBox()
        Me.progressConversao = New System.Windows.Forms.ProgressBar()
        Me.listSaida = New System.Windows.Forms.ListBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.checkEstoque = New System.Windows.Forms.CheckBox()
        Me.checkFornecedor = New System.Windows.Forms.CheckBox()
        Me.checkClientes = New System.Windows.Forms.CheckBox()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        resources.ApplyResources(Me.Button1, "Button1")
        Me.Button1.Name = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txtDataOrigem
        '
        resources.ApplyResources(Me.txtDataOrigem, "txtDataOrigem")
        Me.txtDataOrigem.Name = "txtDataOrigem"
        '
        'btnConverter
        '
        resources.ApplyResources(Me.btnConverter, "btnConverter")
        Me.btnConverter.Name = "btnConverter"
        Me.btnConverter.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel1
        '
        resources.ApplyResources(Me.FlowLayoutPanel1, "FlowLayoutPanel1")
        Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox1)
        Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox2)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        '
        'GroupBox1
        '
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.txtDataOrigem)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'Button2
        '
        resources.ApplyResources(Me.Button2, "Button2")
        Me.Button2.Name = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Controls.Add(Me.Button3)
        Me.GroupBox2.Controls.Add(Me.txtDataSaida)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'Button3
        '
        resources.ApplyResources(Me.Button3, "Button3")
        Me.Button3.Name = "Button3"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'txtDataSaida
        '
        resources.ApplyResources(Me.txtDataSaida, "txtDataSaida")
        Me.txtDataSaida.Name = "txtDataSaida"
        '
        'progressConversao
        '
        Me.progressConversao.AccessibleRole = System.Windows.Forms.AccessibleRole.Alert
        resources.ApplyResources(Me.progressConversao, "progressConversao")
        Me.progressConversao.Name = "progressConversao"
        '
        'listSaida
        '
        resources.ApplyResources(Me.listSaida, "listSaida")
        Me.listSaida.FormattingEnabled = True
        Me.listSaida.Name = "listSaida"
        '
        'GroupBox4
        '
        resources.ApplyResources(Me.GroupBox4, "GroupBox4")
        Me.GroupBox4.Controls.Add(Me.listSaida)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.TabStop = False
        '
        'GroupBox3
        '
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.Controls.Add(Me.checkEstoque)
        Me.GroupBox3.Controls.Add(Me.checkFornecedor)
        Me.GroupBox3.Controls.Add(Me.checkClientes)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'checkEstoque
        '
        resources.ApplyResources(Me.checkEstoque, "checkEstoque")
        Me.checkEstoque.Name = "checkEstoque"
        Me.checkEstoque.UseVisualStyleBackColor = True
        '
        'checkFornecedor
        '
        resources.ApplyResources(Me.checkFornecedor, "checkFornecedor")
        Me.checkFornecedor.Name = "checkFornecedor"
        Me.checkFornecedor.UseVisualStyleBackColor = True
        '
        'checkClientes
        '
        resources.ApplyResources(Me.checkClientes, "checkClientes")
        Me.checkClientes.Name = "checkClientes"
        Me.checkClientes.UseVisualStyleBackColor = True
        '
        'Conversor
        '
        Me.AllowDrop = True
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.progressConversao)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.btnConverter)
        Me.Name = "Conversor"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents txtDataOrigem As TextBox
    Friend WithEvents btnConverter As Button
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Button3 As Button
    Friend WithEvents txtDataSaida As TextBox
    Friend WithEvents progressConversao As ProgressBar
    Friend WithEvents listSaida As ListBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents checkEstoque As CheckBox
    Friend WithEvents checkFornecedor As CheckBox
    Friend WithEvents checkClientes As CheckBox
    Friend WithEvents Button2 As Button
End Class
